﻿using CommandLineRunModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Services
{
    public class ServiceAgentElastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private object serviceLocker = new object();

        private clsTransaction transaction;
        private clsRelationConfig relationConfig;

        private ServiceResult serviceResult_CodeSnipplets;
        public ServiceResult ServiceResult_CondeSnipplet
        {
            get
            {
                lock(serviceLocker)
                {
                    return serviceResult_CodeSnipplets;
                }
                
            }
            set
            {
                lock(serviceLocker)
                {
                    serviceResult_CodeSnipplets = value;
                }
                
                RaisePropertyChanged(nameof(ServiceResult_CondeSnipplet));
            }
        }

        private clsOntologyItem resultSaveCode;
        public clsOntologyItem ResultSaveCode
        {
            get { return resultSaveCode; }
            set
            {
                resultSaveCode = value;
                RaisePropertyChanged(nameof(ResultSaveCode));
            }
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            return dbReader.GetOItem(id, type);
        }

        public async Task<ServiceResult> GetDataCodeSnipplet(clsOntologyItem oItemSnipplet)
        {
            var result = new ServiceResult
            {
                Result = localConfig.Globals.LState_Success.Clone(),
                Snipplet = oItemSnipplet
            };

            var dbReaderCodeSnipplet = new OntologyModDBConnector(localConfig.Globals);

            var searchCode = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_Object = oItemSnipplet.GUID
                }
            };

            result.Result = dbReaderCodeSnipplet.GetDataObjectAtt(searchCode);

            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ServiceResult_CondeSnipplet = result;
                return ServiceResult_CondeSnipplet;
            }

            result.Code = dbReaderCodeSnipplet.ObjAtts.FirstOrDefault();

            var dbReaderProgrammingLanguage = new OntologyModDBConnector(localConfig.Globals);

            var searchProgLang = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemSnipplet.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_is_written_in.GUID,
                    ID_Parent_Other = localConfig.OItem_class_programing_language.GUID
                }
            };

            result.Result = dbReaderProgrammingLanguage.GetDataObjectRel(searchProgLang);

            if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ServiceResult_CondeSnipplet = result;
                return ServiceResult_CondeSnipplet;
            }

            result.SnippletToProgrammingLanguage = dbReaderProgrammingLanguage.ObjectRels.FirstOrDefault();

            if (result.SnippletToProgrammingLanguage != null)
            {
                var dbReaderSyntaxHighlighting = new OntologyModDBConnector(localConfig.Globals);

                var searchCodeHighlighting = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = result.SnippletToProgrammingLanguage.ID_Other,
                        ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                        ID_Parent_Object = localConfig.OItem_class_syntax_highlighting__ace_.GUID
                    }
                };

                result.Result = dbReaderSyntaxHighlighting.GetDataObjectRel(searchCodeHighlighting);

                if (result.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    ServiceResult_CondeSnipplet = result;
                    return ServiceResult_CondeSnipplet;
                }

                result.SyntaxHighlightingToProgrammingLanguage = dbReaderSyntaxHighlighting.ObjectRels.FirstOrDefault();
            }

            var dbReaderVariables = new OntologyModDBConnector(localConfig.Globals);

            var searchVariables = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemSnipplet.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = localConfig.OItem_class_variable.GUID
                }
            };

            result.Result = dbReaderVariables.GetDataObjectRel(searchVariables);

            if (result.Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result.SnippletToVariables = dbReaderVariables.ObjectRels;
            }

            ServiceResult_CondeSnipplet = result;
            return ServiceResult_CondeSnipplet;
        }

        public async Task<clsOntologyItem> SaveScript(Script scriptItem, clsOntologyItem oItemSnipplet)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            if (scriptItem == null || 
                string.IsNullOrEmpty(scriptItem.IdAttributeCode) || 
                !localConfig.Globals.is_GUID(scriptItem.IdAttributeCode) ||
                string.IsNullOrEmpty(scriptItem.IdCodeSnipplet) ||
                !localConfig.Globals.is_GUID(scriptItem.IdCodeSnipplet))
            {
                result = localConfig.Globals.LState_Error.Clone();
                return result;
            }

            lock(serviceLocker)
            {
                var saveItem = relationConfig.Rel_ObjectAttribute(oItemSnipplet, localConfig.OItem_attributetype_code, scriptItem.Code);
                transaction.ClearItems();
                result = transaction.do_Transaction(saveItem, true);
            }
            

            ResultSaveCode = result;
            return result;
        }

        public ServiceAgentElastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            transaction = new clsTransaction(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
        }
    }

    public class ServiceResult
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem Snipplet { get; set; }
        public clsObjectAtt Code { get; set; }
        public clsObjectRel SnippletToProgrammingLanguage { get; set; }
        public clsObjectRel SyntaxHighlightingToProgrammingLanguage { get; set; }
        public List<clsObjectRel> SnippletToVariables { get; set; }
    }
}
