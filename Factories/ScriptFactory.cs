﻿using CommandLineRunModule.Models;
using CommandLineRunModule.Services;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Factories
{

    public class ScriptFactory : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        private object factoryLocker = new object();

        private FactoryResult factoryResult;
        public FactoryResult FactoryResult
        {
            get
            {
                lock(factoryLocker)
                {
                    return factoryResult;
                }
            }
            set
            {
                lock(factoryLocker)
                {
                    factoryResult = value;
                }
                RaisePropertyChanged(nameof(FactoryResult));
            }
        }


        public FactoryResult GetScript(ServiceResult serviceResult, SessionFile sessionFile)
        {
            var result = new FactoryResult
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            if (serviceResult.Code != null)
            {
                using (sessionFile.StreamWriter)
                {
                    sessionFile.StreamWriter.Write(serviceResult.Code.Val_String);
                }
            }

            var script = new Script
            {
                IdCodeSnipplet = serviceResult.Snipplet.GUID,
                NameCodeSnipplet = serviceResult.Snipplet.Name,
                IdSyntaxHighlighting = serviceResult.SyntaxHighlightingToProgrammingLanguage != null ? serviceResult.SyntaxHighlightingToProgrammingLanguage.ID_Object : null,
                Mode = serviceResult.SyntaxHighlightingToProgrammingLanguage != null ?  serviceResult.SyntaxHighlightingToProgrammingLanguage.Name_Object : null,
                IdProgrammingLanguage = serviceResult.SnippletToProgrammingLanguage != null ?  serviceResult.SnippletToProgrammingLanguage.ID_Other : null,
                NameProgrammingLanguage = serviceResult.SnippletToProgrammingLanguage != null ? serviceResult.SnippletToProgrammingLanguage.Name_Other : null,
                IdAttributeCode = serviceResult.Code != null ? serviceResult.Code.ID_Attribute : null,
                Variables = serviceResult.SnippletToVariables.Select(snipToVar => new clsOntologyItem
                {
                    GUID = snipToVar.ID_Other,
                    Name = snipToVar.Name_Other,
                    GUID_Parent = localConfig.OItem_class_code_snipplets.GUID,
                    Type = localConfig.Globals.Type_Object
                }).ToList(),
                UrlScript = sessionFile.FileUri.AbsoluteUri
            };

            result.Script = script;

            FactoryResult = result;
            return FactoryResult;
        }

        public ScriptFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }

    public class FactoryResult
    {
        public clsOntologyItem Result { get; set; }
        public Script Script { get; set; }
    }
}
