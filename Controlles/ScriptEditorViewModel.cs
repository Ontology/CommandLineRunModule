﻿using CommandLineRunModule.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Controlles
{
    public class ScriptEditorViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(nameof(IsSuccessful_Login));

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(nameof(IsToggled_Listen));

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(nameof(Text_View));

            }
        }

        private Script script_Script;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "script", ViewItemType = ViewItemType.Other)]
		public Script Script_Script
        {
            get { return script_Script; }
            set
            {
                if (script_Script == value) return;

                script_Script = value;

                RaisePropertyChanged(nameof(Script_Script));

            }
        }

        private bool isenabled_Save;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "btnSave", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Save
        {
            get { return isenabled_Save; }
            set
            {

                isenabled_Save = value;

                RaisePropertyChanged(nameof(IsEnabled_Save));

            }
        }

        private Script script_ScriptChange;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "script", ViewItemType = ViewItemType.Content)]
		public Script Script_ScriptChange
        {
            get { return script_ScriptChange; }
            set
            {
                if (script_ScriptChange == value) return;

                script_ScriptChange = value;

                RaisePropertyChanged(nameof(Script_ScriptChange));

            }
        }
    }
}
