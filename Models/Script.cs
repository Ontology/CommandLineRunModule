﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRunModule.Models
{
    public class Script
    {
        public string IdCodeSnipplet { get; set; }
        public string NameCodeSnipplet { get; set; }
        public string IdProgrammingLanguage { get; set; }
        public string NameProgrammingLanguage { get; set; }
        public string IdSyntaxHighlighting { get; set; }
        public string IdAttributeCode { get; set; }
        public string Mode { get; set; }
        public string UrlScript { get; set; }
        public string Code { get; set; }
        public List<clsOntologyItem> Variables { get; set; }
    }
}
