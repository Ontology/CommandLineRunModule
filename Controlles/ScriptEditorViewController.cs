﻿using CommandLineRunModule.Factories;
using CommandLineRunModule.Models;
using CommandLineRunModule.Services;
using CommandLineRunModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CommandLineRunModule.Controlles
{
    public class ScriptEditorViewController : ScriptEditorViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private clsTransaction transactionManager;

        private TranslationController translationController = new TranslationController();

        private ServiceAgentElastic serviceAgentElastic;
        private ScriptFactory scriptFactory;

        private clsLocalConfig localConfig;

        private clsOntologyItem oItemScript;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public ScriptEditorViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += ScriptEditorViewController_PropertyChanged;
        }

        private void Initialize()
        {

            transactionManager = new clsTransaction(localConfig.Globals);

            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;

            scriptFactory = new ScriptFactory(localConfig);
            scriptFactory.PropertyChanged += ScriptFactory_PropertyChanged;

            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            webSocketServiceAgent.SendModel();

            IsEnabled_Save = false;

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            IsToggled_Listen = true;
            
            Text_View = translationController.Text_View;
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            if (oItemSelected.GUID_Parent != localConfig.OItem_class_code_snipplets.GUID) return;

            IsToggled_Listen = false;
            IsEnabled_Save = false;
            oItemScript = oItemSelected;

            Text_View = oItemScript.Name;

            var resultTask = serviceAgentElastic.GetDataCodeSnipplet(oItemScript);
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ScriptFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ScriptFactory.FactoryResult))
            {
                if (scriptFactory.FactoryResult.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Script_Script = scriptFactory.FactoryResult.Script;
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ServiceAgentElastic.ServiceResult_CondeSnipplet))
            {
                if (serviceAgentElastic.ServiceResult_CondeSnipplet.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(localConfig.Globals.NewGUID + ".txt");

                    var resultTask = scriptFactory.GetScript(serviceAgentElastic.ServiceResult_CondeSnipplet, sessionFile);
                }
            }
            else if (e.PropertyName == nameof(ServiceAgentElastic.ResultSaveCode))
            {
                if (serviceAgentElastic.ResultSaveCode.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    IsEnabled_Save = false;
                    webSocketServiceAgent.SendCommand("script.saved");
                }
            }

        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

        

        private void ScriptEditorViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));


            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

       
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {

                Initialize();
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "clicked.listen")
                {
                    IsToggled_Listen = !IsToggled_Listen;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "code.changed")
                {

                    IsEnabled_Save = true;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "clicked.save")
                {
                    webSocketServiceAgent.SendCommand("send.content");
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "code.reload")
                {
                    LocalStateMachine.SetItemSelected(oItemScript);
                }
                


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "script" && changedItem.ViewItemType == "Content")
                    {
                        var scriptItem = Newtonsoft.Json.JsonConvert.DeserializeObject<Script>(changedItem.ViewItemValue.ToString());

                        scriptItem.IdCodeSnipplet = oItemScript.GUID;

                        var resultTask = serviceAgentElastic.SaveScript(scriptItem, oItemScript);

                    }
                });


            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var item = serviceAgentElastic.GetOItem(webSocketServiceAgent.ObjectArgument.Value.ToString(), localConfig.Globals.Type_Object);
                if (item != null && item.GUID_Related == localConfig.Globals.LState_Success.GUID)
                {
                    LocalStateMachine.SetItemSelected(item);
                }
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (!IsToggled_Listen) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                if (message.OItems != null && message.OItems.Any())
                {
                    var paramObject = serviceAgentElastic.GetOItem(message.OItems.First().GUID, localConfig.Globals.Type_Object);
                    LocalStateMachine.SetItemSelected(paramObject);
                    
                }
            }

        }

        

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
